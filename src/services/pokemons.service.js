import axios from "axios"

export const loadPokemon = async id => {
  const { data } = await axios.get(`https://pokeapi.co/api/v2/pokemon/${id}`)
  return {
    id,
    name: data.name,
    img: data.sprites.front_default
  }
}
