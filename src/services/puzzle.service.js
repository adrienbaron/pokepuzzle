import { loadPokemon } from "./pokemons.service"

const MAX_POKEMON_ID = 251

export const createPuzzleGrid = async (xSize, ySize) => {
  const tileCount = xSize * ySize
  if (tileCount % 2 === 1) {
    throw new Error("Puzzle tile count must be even")
  }
  const pairCount = tileCount / 2
  if (pairCount > MAX_POKEMON_ID) {
    throw new Error("Puzzle is bigger than max amount of pokemons")
  }
  const pokemonIds = generateRandomPokemonIds(pairCount)
  const pokemons = await Promise.all(pokemonIds.map(loadPokemon))

  const availablePuzzleTiles = pokemons.reduce(
    (result, pokemon) => [
      ...result,
      { tileId: `1_${pokemon.id}`, pokemon, isVisible: false },
      { tileId: `2_${pokemon.id}`, pokemon, isVisible: false }
    ],
    []
  )
  const puzzleGrid = []
  for (let i = 0; i < ySize; i++) {
    puzzleGrid[i] = []
    for (let j = 0; j < xSize; j++) {
      const availablePuzzleTileIndex = Math.floor(
        Math.random() * availablePuzzleTiles.length
      )
      puzzleGrid[i][j] = availablePuzzleTiles[availablePuzzleTileIndex]
      availablePuzzleTiles.splice(availablePuzzleTileIndex, 1)
    }
  }
  return puzzleGrid
}

const generateRandomPokemonIds = count => {
  const availablePokemonIds = [...Array(MAX_POKEMON_ID).keys()]
  availablePokemonIds.unshift()

  const pokemonPairsId = []
  for (let i = 0; i < count; i++) {
    const availablePokemonIndex = Math.floor(
      Math.random() * availablePokemonIds.length
    )
    pokemonPairsId.push(availablePokemonIds[availablePokemonIndex])
    availablePokemonIds.splice(availablePokemonIndex, 1)
  }
  return pokemonPairsId
}
