import Vue from "vue"
import Vuex from "vuex"
import { LOAD_GRID, SELECT_TILE } from "./actions"
import {
  LOAD_GRID_ERROR,
  LOAD_GRID_REQUEST,
  LOAD_GRID_SUCCESS,
  SELECTED_PAIR_ADD,
  SELECTED_PAIR_CLEAR,
  SELECTED_PAIR_SUCCESS,
  SET_GRID_SIZE
} from "./mutations"
import { createPuzzleGrid } from "../services/puzzle.service"

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    grid: null,
    gridSize: { x: 4, y: 4 },
    isLoading: false,
    errorMessage: null,
    selectedPair: [],
    foundTiles: []
  },
  mutations: {
    [LOAD_GRID_REQUEST](state) {
      state.grid = null
      state.isLoading = true
      state.errorMessage = null
    },
    [LOAD_GRID_SUCCESS](state, { grid }) {
      state.grid = grid
      state.isLoading = false
      state.errorMessage = null
    },
    [LOAD_GRID_ERROR](state, { errorMessage }) {
      state.grid = null
      state.isLoading = false
      state.errorMessage = errorMessage
    },
    [SET_GRID_SIZE](state, { x, y }) {
      state.gridSize.x = x
      state.gridSize.y = y
    },
    [SELECTED_PAIR_ADD](state, { tile }) {
      state.selectedPair.push(tile)
    },
    [SELECTED_PAIR_SUCCESS](state) {
      state.foundTiles.push(...state.selectedPair)
      state.selectedPair = []
    },
    [SELECTED_PAIR_CLEAR](state) {
      state.selectedPair = []
    }
  },
  actions: {
    async [LOAD_GRID]({ state, commit }, { x, y }) {
      commit(SET_GRID_SIZE, { x, y })
      commit(LOAD_GRID_REQUEST)

      const xSize = state.gridSize.x
      const ySize = state.gridSize.y
      try {
        const grid = await createPuzzleGrid(xSize, ySize)
        if (xSize !== state.gridSize.x || ySize !== state.gridSize.y) {
          return
        }
        commit(LOAD_GRID_SUCCESS, { grid })
      } catch (e) {
        commit(LOAD_GRID_ERROR, { errorMessage: e.message })
      }
    },
    async [SELECT_TILE]({ state, commit }, { tile }) {
      if (state.selectedPair.length === 2 || state.foundTiles.includes(tile)) {
        return
      }
      if (state.selectedPair.includes(tile)) {
        commit(SELECTED_PAIR_CLEAR)
        return
      }

      commit(SELECTED_PAIR_ADD, { tile })
      if (state.selectedPair.length === 2) {
        const [tile1, tile2] = state.selectedPair
        if (tile1.pokemon.id === tile2.pokemon.id) {
          setTimeout(() => commit(SELECTED_PAIR_SUCCESS), 500)
        } else {
          setTimeout(() => commit(SELECTED_PAIR_CLEAR), 1000)
        }
      }
    }
  }
})
